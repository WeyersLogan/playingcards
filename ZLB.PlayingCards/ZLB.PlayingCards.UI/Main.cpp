//Playing Cards

// Zachary Brandt
// Logan's Fork

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum Rank {Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace};

enum Suit {Hearts, Spades, Clubs, Diamonds};

struct Card
{
	Rank rank;
	Suit suit;
};

void PrintCard(Rank r, Suit s) 
{
	cout << "The ";
	switch (r)
	{
	case Two: cout << "Two";
		break;
	case Three: cout << "Three";
		break;
	case Four:  cout << "Four";
		break;
	case Five: cout << "Five";
		break;
	case Six:  cout << "Six";
		break;
	case Seven: cout << "Seven";
		break;
	case Eight: cout << "Eight";
		break;
	case Nine: cout << "Nine";
		break;
	case Ten:  cout << "Ten";
		break;
	case Jack: cout << "Jack";
		break;
	case Queen: cout << "Queen";
		break;
	case King: cout << "King";
		break;
	case Ace: cout << "Ace";
		break;
	}

	cout << " of ";

	switch (s)
	{
	case Clubs: cout << "Clubs";
		break;
	case Diamonds: cout << "Diamonds";
		break;
	case Hearts: cout << "Hearts";
		break;
	case Spades: cout << "Spades";
		break;
	}

	cout << "!!!";
	
}

Card HighCard(Card first, Card second) 
{
	if (first.rank > second.rank) 
	{
		return first;
	}
	else if (second.rank > first.rank) 
	{
		return second;
	}
	else
	{
		// In case of a tie
		return first;
	}
}

int main()
{
	Card card1;
	card1.rank = Jack;
	card1.suit = Spades;

	PrintCard(card1.rank, card1.suit);

	Card lowCard;
	lowCard.rank = Two;
	lowCard.suit = Hearts;

	Card highCard;
	highCard.rank = Nine;
	highCard.suit = Diamonds;

	HighCard(highCard, lowCard);


	(void)_getch();
	return 0;
}
